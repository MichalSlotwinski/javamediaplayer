# get alpine based gitlab-runner image
FROM gitlab/gitlab-runner:alpine
# upgrade to alpine edge version
RUN echo $'http://dl-cdn.alpinelinux.org/alpine/edge/main\nhttp://dl-cdn.alpinelinux.org/alpine/edge/community' > /etc/apk/repositories
RUN apk update
RUN apk add --upgrade apk-tools
RUN apk upgrade --available
RUN sync
# install maven
RUN apk add maven
# make gitlab-runner default option
CMD ["run","--user=gitlab-runner","--working-directory=/home/gitlab-runner"]