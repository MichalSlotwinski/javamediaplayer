package pl.rabbit.media.player;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.rabbit.media.player.view.MainView;

/**
 * JavaFX App
 */
public class App extends Application
{
    private MainView view;
    
    @Override
    public void start(Stage primaryStage)
    {
        this.view = new MainView(primaryStage);
        this.view.show();
    }

    public static void main(String[] args) {
        launch();
    }

}