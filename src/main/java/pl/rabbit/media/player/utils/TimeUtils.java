package pl.rabbit.media.player.utils;

import javafx.util.Duration;

public final class TimeUtils
{
    public static String getTime(Duration duration)
    {
        int hours = (int)duration.toHours();
        int minutes = (int)duration.toMinutes() % 60;
        int seconds = (int)duration.toSeconds() % 60;
        if(hours == 0)
            return String.format("%02d:%02d",
                                 (int)duration.toMinutes() % 60,
                                 (int)duration.toSeconds() % 60
            );
        else
            return String.format("%d:%02d:%02d",
                                 (int)duration.toHours(),
                                 (int)duration.toMinutes() % 60,
                                 (int)duration.toSeconds() % 60
            );
    }
}
