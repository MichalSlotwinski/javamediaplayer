package pl.rabbit.media.player.view;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.handlers.*;

public class MainView
{
    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;
    private static final int MIN_WIDTH = 290;
    private static final int MIN_HEIGHT = 97;
    
    private ChangeListener<Number> paneWidthChangeListener;
    private ChangeListener<Number> paneHeightChangeListener;
    
    private Stage primaryStage;
    private Scene scene;
    private BorderPane layout;

    private MenuView menu;
    private ControlsView controls;
    private MediaView media;
    
    private MediaController controller;
    
    public MainView(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
        this.layout = new BorderPane();
        
        this.menu = new MenuView();
        this.menu.setOnExitAction(new ExitHandler());
        this.menu.init();
        this.layout.setTop(this.menu.view());
        
        this.controls = new ControlsView();
        this.controls.init();
        this.layout.setBottom(this.controls.view());
        
        this.media = new MediaView();
        this.layout.setCenter(this.media.view());
        
        this.controller = new MediaController();
        this.menu.setOnOpenFileAction(new OpenFileHandler(this.controller, this.controls, this.media, this.primaryStage));
        
        this.primaryStage.setOnCloseRequest(
                event ->
                {
                    Platform.exit();
                    System.exit(0);
                });
        this.scene = new Scene(this.layout);
        this.setPaneWidthChangeListener(new PaneWidthHandler(this.media.view()));
        this.setPaneHeightChangeListener(new PaneHeightHandler(this.media.view()));
        this.primaryStage.setScene(this.scene);
        this.primaryStage.setMinWidth(MIN_WIDTH);
        this.primaryStage.setWidth(WIDTH);
        this.primaryStage.setMinHeight(MIN_HEIGHT);
        this.primaryStage.setHeight(HEIGHT);
        this.primaryStage.addEventHandler(KeyEvent.KEY_PRESSED, new FullScreenHandler(this.primaryStage));
    }
    
    public void setPaneWidthChangeListener(ChangeListener<Number> listener)
    {
        if(this.paneHeightChangeListener != null)
            this.scene.widthProperty().removeListener(this.paneWidthChangeListener);
        this.paneWidthChangeListener = listener;
        this.scene.widthProperty().addListener(this.paneWidthChangeListener);
    }
    
    public void setPaneHeightChangeListener(ChangeListener<Number> listener)
    {
        if(this.paneHeightChangeListener != null)
            this.scene.heightProperty().removeListener(this.paneHeightChangeListener);
        this.paneHeightChangeListener = listener;
        this.scene.heightProperty().addListener(this.paneHeightChangeListener);
    }
    
    public void show()
    {
        this.primaryStage.show();
    }
}
