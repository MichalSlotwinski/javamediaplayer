package pl.rabbit.media.player.view;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class MediaView
{
    private ChangeListener<Number> viewportWidthResizeListener;
    private ChangeListener<Number> viewportHeightResizeListener;
    
    private HBox layout;
    private javafx.scene.media.MediaView view;
    
    public MediaView()
    {
        this.layout = new HBox();
        this.layout.setAlignment(Pos.CENTER);
        this.layout.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
    }
    
    public HBox view()
    {
        return this.layout;
    }
    
    public void setView(javafx.scene.media.MediaView node)
    {
        this.view = node;
        this.layout.getChildren().clear();
        this.layout.getChildren().add(this.view);
        this.layout.resize(this.view.getFitWidth(), this.view.getFitHeight());
    }
    
    public void setOnViewportWidthChangeAction(ChangeListener<Number> listener)
    {
        if(viewportWidthResizeListener != null)
            this.layout.widthProperty().removeListener(this.viewportWidthResizeListener);
        this.viewportWidthResizeListener = listener;
        this.layout.widthProperty().addListener(this.viewportWidthResizeListener);
    }
    
    public void setOnViewportHeightChangeAction(ChangeListener<Number> listener)
    {
        if(viewportHeightResizeListener != null)
            this.layout.heightProperty().removeListener(this.viewportHeightResizeListener);
        this.viewportHeightResizeListener = listener;
        this.layout.heightProperty().addListener(this.viewportHeightResizeListener);
    }
    
    public void setDefaultView()
    {
        this.layout.getChildren().clear();
    }
    
    public void rollbackView()
    {
        if(!this.layout.getChildren().isEmpty())
            this.layout.getChildren().clear();
        this.layout.getChildren().add(this.view);
    }
}
