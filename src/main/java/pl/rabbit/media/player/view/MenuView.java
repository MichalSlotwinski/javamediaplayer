package pl.rabbit.media.player.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;

public class MenuView
{
    private EventHandler<ActionEvent> exitHandler;
    private EventHandler<ActionEvent> openFileHandler;
    
    private final MenuBar menuBar;
    private final javafx.scene.control.Menu file;
    private final MenuItem openFile;
    private final MenuItem exit;
    
    public MenuView()
    {
        this.menuBar = new MenuBar();
        this.file = new javafx.scene.control.Menu();
        this.openFile = new MenuItem();
        this.exit = new MenuItem();
    }
    
    public MenuBar view()
    {
        return menuBar;
    }
    
    public void setOnExitAction(EventHandler<ActionEvent> eventHandler)
    {
        this.exitHandler = eventHandler;
        this.exit.setOnAction(this.exitHandler);
    }
    
    public void setOnOpenFileAction(EventHandler<ActionEvent> eventHandler)
    {
        this.openFileHandler = eventHandler;
        this.openFile.setOnAction(this.openFileHandler);
    }
    
    public void init()
    {
        initFile();
    }
    
    private void initFile()
    {
        this.file.setText("File");
        this.openFile.setText("Open file...");
        this.exit.setText("Exit");
        this.file.getItems().addAll(this.openFile, this.exit);
        this.menuBar.getMenus().add(this.file);
    }
    
    public void removeExitAction()
    {
        this.exit.setOnAction(null);
    }
    
    public void removeOpenFileAction()
    {
        this.openFile.setOnAction(null);
    }
}
