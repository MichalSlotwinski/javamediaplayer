package pl.rabbit.media.player.view;

import javafx.geometry.Pos;
import javafx.scene.layout.*;
import pl.rabbit.media.player.controller.PlayController;
import pl.rabbit.media.player.controller.ProgressController;
import pl.rabbit.media.player.controller.VolumeController;

public class ControlsView
{
    private VBox layout;
    private HBox lowerLayout;
    
    private PlayController play;
    private ProgressController progress;
    private VolumeController volume;
    
    private Region filler;
    
    public ControlsView()
    {
        this.layout = new VBox();
        this.lowerLayout = new HBox();
        
        this.progress = new ProgressController();
        this.play = new PlayController();
        this.volume = new VolumeController();
        this.filler = new Region();
        
        HBox.setHgrow(this.filler, Priority.ALWAYS);
        this.lowerLayout.getChildren().addAll(this.play.layout(), this.filler, this.volume.layout());
        this.lowerLayout.setAlignment(Pos.CENTER);
        this.layout.getChildren().addAll(this.progress.layout(), this.lowerLayout);
        this.layout.setAlignment(Pos.CENTER);
    }
    
    public VBox view()
    {
        return layout;
    }
    
    public PlayController getPlay()
    {
        return play;
    }
    
    public ProgressController getProgress()
    {
        return progress;
    }
    
    public VolumeController getVolume()
    {
        return volume;
    }
    
    public void init()
    {
        this.play.init();
        this.progress.init();
        this.volume.init();
    }
}
