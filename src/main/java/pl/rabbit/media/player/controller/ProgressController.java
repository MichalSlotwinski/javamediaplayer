package pl.rabbit.media.player.controller;

import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.Duration;
import pl.rabbit.media.player.utils.TimeUtils;

public class ProgressController
{
    public static final String DEFAULT = "00:00";
    
    private EventHandler<MouseEvent> clickedHandler;
    private EventHandler<MouseEvent> pressedHandler;
    private EventHandler<MouseEvent> releasedHandler;
    private EventHandler<MouseEvent> draggedHandler;
    
    private Slider progress;
    private Label time;
    private Label duration;
    
    private HBox layout;
    
    public ProgressController()
    {
        this.progress = new Slider();
        this.time = new Label();
        this.duration = new Label();
        
        this.layout = new HBox();
        this.layout.getChildren().addAll(this.time, this.progress, this.duration);
    }
    
    public void init()
    {
        HBox.setHgrow(this.progress, Priority.ALWAYS);
        this.progress.setOrientation(Orientation.HORIZONTAL);
        this.progress.setValue(0);
        this.time.setText(DEFAULT);
        this.duration.setText(DEFAULT);
    }
    
    public HBox layout()
    {
        return layout;
    }
    
    public Slider getProgressSlider()
    {
        return progress;
    }
    
    public Label getTimeLabel()
    {
        return time;
    }
    
    public Label getDurationLabel()
    {
        return duration;
    }
    
    public void setDuration(Duration duration)
    {
        this.progress.setMin(0);
        this.progress.setMax(duration.toSeconds());
        this.progress.setValue(0);
        this.duration.setText(TimeUtils.getTime(duration));
    }
    
    public void setDefaultTimeLabel()
    {
        this.time.setText(DEFAULT);
        this.duration.setText(DEFAULT);
    }
    
    public void setOnClickedAction(EventHandler<MouseEvent> eventHandler)
    {
        this.clickedHandler = eventHandler;
        this.progress.setOnMouseClicked(this.clickedHandler);
    }
    
    public void setOnPressedAction(EventHandler<MouseEvent> eventHandler)
    {
        this.pressedHandler = eventHandler;
        this.progress.setOnMousePressed(this.pressedHandler);
    }
    
    public void setOnReleasedAction(EventHandler<MouseEvent> eventHandler)
    {
        this.releasedHandler = eventHandler;
        this.progress.setOnMouseReleased(this.releasedHandler);
    }
    
    public void setOnDraggedAction(EventHandler<MouseEvent> eventHandler)
    {
        this.draggedHandler = eventHandler;
        this.progress.setOnMouseDragged(this.draggedHandler);
    }
    
    public void removeClickedAction()
    {
        setOnClickedAction(null);
    }
    
    public void removePressedAction()
    {
        setOnPressedAction(null);
    }
    
    public void removeReleasedAction()
    {
        setOnReleasedAction(null);
    }
    
    public void removeDraggedAction()
    {
        setOnDraggedAction(null);
    }
}
