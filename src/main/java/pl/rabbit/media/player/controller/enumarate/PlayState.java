package pl.rabbit.media.player.controller.enumarate;

public enum PlayState
{
    PLAY("Play"),
    PAUSE("Pause"),
    STOP("Stop");
    
    private final String text;
    
    PlayState(String text)
    {
        this.text = text;
    }
    
    public String getText()
    {
        return this.text;
    }
}
