package pl.rabbit.media.player.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import pl.rabbit.media.player.controller.enumarate.PlayState;

public class PlayController
{
    private final int buttonSize = 60;
    
    private EventHandler<ActionEvent> playHandler;
    private EventHandler<ActionEvent> stopHandler;

    private Button play;
    private Button stop;
    
    private HBox layout;
    
    public PlayController()
    {
        this.play = new Button();
        this.stop = new Button();
        
        this.layout = new HBox();
        this.layout.getChildren().addAll(this.play, this.stop);
    }
    
    public void init()
    {
        this.play.setText(PlayState.PLAY.getText());
        this.play.setMinSize(this.buttonSize, this.buttonSize);
        this.play.setPrefSize(this.buttonSize, this.buttonSize);
        this.play.setMaxSize(this.buttonSize, this.buttonSize);
        this.stop.setText(PlayState.STOP.getText());
        this.stop.setMinSize(this.buttonSize, this.buttonSize);
        this.stop.setPrefSize(this.buttonSize, this.buttonSize);
        this.stop.setMaxSize(this.buttonSize, this.buttonSize);
    }
    
    public void play()
    {
        this.play.setText(PlayState.PAUSE.getText());
    }
    
    public void pause()
    {
        this.play.setText(PlayState.PLAY.getText());
    }
    
    public void stop()
    {
        this.play.setText(PlayState.PLAY.getText());
    }
    
    public HBox layout()
    {
        return layout;
    }
    
    public Button getPlayButton()
    {
        return play;
    }
    
    public Button getStopButton()
    {
        return stop;
    }
    
    public void setOnPlayAction(EventHandler<ActionEvent> eventHandler)
    {
        this.playHandler = eventHandler;
        this.play.setOnAction(this.playHandler);
    }
    
    public void setOnStopAction(EventHandler<ActionEvent> eventHandler)
    {
        this.stopHandler = eventHandler;
        this.stop.setOnAction(this.stopHandler);
    }
    
    public void removePlayAction()
    {
        setOnPlayAction(null);
    }
    
    public void removeStopAction()
    {
        setOnStopAction(null);
    }
}
