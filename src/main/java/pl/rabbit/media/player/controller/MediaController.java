package pl.rabbit.media.player.controller;

import javafx.beans.value.ChangeListener;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

import java.net.URI;

public class MediaController
{
    private ChangeListener<? super Duration> progressListener;
    
    private Media media;
    private MediaPlayer player;
    private MediaView view;
    
    public MediaController()
    {
        this.media = null;
        this.player = null;
        this.view = new MediaView();
    }
    
    public Media media()
    {
        return media;
    }
    
    public MediaPlayer player()
    {
        return player;
    }
    
    public MediaView view()
    {
        return view;
    }
    
    public void open(URI uri)
    {
        this.media = new Media(uri.toString());
        this.player = new MediaPlayer(this.media);
        this.view.setMediaPlayer(this.player);
    }
    
    public void setOnProgressChangeListener(ChangeListener<? super Duration> listener)
    {
        if(this.progressListener != null)
            this.player.currentTimeProperty().removeListener(this.progressListener);
        this.progressListener = listener;
        this.player.currentTimeProperty().addListener(this.progressListener);
    }
    
    public void reset()
    {
        this.media = null;
        this.player = null;
        this.view = new MediaView();
    }
}
