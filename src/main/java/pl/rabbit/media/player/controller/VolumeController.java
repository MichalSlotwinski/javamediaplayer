package pl.rabbit.media.player.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import pl.rabbit.media.player.controller.enumarate.VolumeState;

public class VolumeController
{
    public static final String VOLUME_PERCENTAGE_FORMAT = "%d%%";
    private final int labelSize = 12;
    private final int buttonSize = 60;
    
    private EventHandler<ActionEvent> muteHandler;
    
    private Button mute;
    private Slider volume;
    private Label volumePercentage;
    
    private double limit;
    
    private VBox volumeSlider;
    private HBox layout;
    
    public VolumeController()
    {
        this.mute = new Button();
        this.volume = new Slider();
        this.volumePercentage = new Label();
        
        this.limit = 1.0;
        
        this.volumeSlider = new VBox();
        this.volumeSlider.getChildren().addAll(this.volumePercentage, this.volume);
        this.volumeSlider.setAlignment(Pos.CENTER);
        
        this.layout = new HBox();
        this.layout.getChildren().addAll(this.mute, this.volumeSlider);
    }
    
    public void init()
    {
        this.mute.setText(VolumeState.MUTED.getText());
        this.mute.setMinSize(this.buttonSize, this.buttonSize);
        this.mute.setPrefSize(this.buttonSize, this.buttonSize);
        this.mute.setMaxSize(this.buttonSize, this.buttonSize);
        this.volume.setOrientation(Orientation.HORIZONTAL);
        this.volumePercentage.setText("0%");
        this.volumePercentage.setMinHeight(this.labelSize);
        this.volumePercentage.setPrefHeight(this.labelSize);
        this.volumePercentage.setMaxHeight(this.labelSize);
        this.volumePercentage.setFont(new Font(this.labelSize));
    }
    
    public void setLimit(double limit)
    {
        this.limit = limit;
        this.volume.setMin(0);
        this.volume.setMax(this.limit);
        this.volume.setValue(this.limit);
        this.mute.setText(VolumeState.HIGH.getText());
        this.volumePercentage.setText(VOLUME_PERCENTAGE_FORMAT.formatted((int)(this.volume.getValue() * this.limit * 100)));
    }
    
    public void setMute(boolean mute)
    {
        if(mute)
            this.mute.setText(VolumeState.MUTED.getText());
        else
            this.mute.setText(
                    VolumeState.stateOf(this.volume.getValue(), this.limit).getText()
            );
    }
    
    public Button getMuteButton()
    {
        return mute;
    }
    
    public Slider getVolumeSlider()
    {
        return volume;
    }
    
    public Label getVolumeLabel()
    {
        return volumePercentage;
    }
    
    public double getLimit()
    {
        return limit;
    }
    
    public HBox layout()
    {
        return layout;
    }
    
    public void setOnMuteAction(EventHandler<ActionEvent> eventHandler)
    {
        this.muteHandler = eventHandler;
        this.mute.setOnAction(this.muteHandler);
    }
    
    public void removeMuteAction()
    {
        setOnMuteAction(null);
    }
}
