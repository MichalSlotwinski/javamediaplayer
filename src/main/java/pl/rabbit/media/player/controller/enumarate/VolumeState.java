package pl.rabbit.media.player.controller.enumarate;

public enum VolumeState
{
    MUTED("<x"),
    LOW("<)"),
    MIDDLE("<))"),
    HIGH("<)))");
    
    private final String text;
    
    VolumeState(String text)
    {
        this.text = text;
    }
    
    public String getText()
    {
        return this.text;
    }
    
    public static VolumeState stateOf(double volume, double max)
    {
        double muted = 0.0;
        double low = max*0.33;
        double middle = max*0.66;
        
        if(volume <= 0.0)
            return MUTED;
        else if(volume > muted && volume <= low)
            return LOW;
        else if(volume > low && volume <= middle)
            return MIDDLE;
        else
            return HIGH;
    }
}
