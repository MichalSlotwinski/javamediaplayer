package pl.rabbit.media.player.handlers;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class FullScreenHandler implements EventHandler<KeyEvent>
{
    private KeyCombination combination = new KeyCodeCombination(KeyCode.F);
    private Stage primaryStage;
    
    public FullScreenHandler(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
    }
    
    @Override
    public void handle(KeyEvent event)
    {
        if(this.combination.match(event) && !this.primaryStage.isFullScreen())
            this.primaryStage.setFullScreen(true);
    }
}
