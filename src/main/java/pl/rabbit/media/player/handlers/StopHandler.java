package pl.rabbit.media.player.handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.controller.PlayController;
import pl.rabbit.media.player.controller.ProgressController;
import pl.rabbit.media.player.view.MediaView;

public class StopHandler implements EventHandler<ActionEvent>, Runnable
{
    private final MediaController media;
    private final MediaView view;
    private final PlayController play;
    private final ProgressController progress;
    
    public StopHandler(MediaController media, MediaView view, PlayController play, ProgressController progress)
    {
        this.media = media;
        this.view = view;
        this.play = play;
        this.progress = progress;
    }
    
    @Override
    public void handle(ActionEvent event)
    {
        this.play.stop();
        this.media.player().stop();
        this.view.setDefaultView();
        this.progress.setDefaultTimeLabel();
    }
    
    @Override
    public void run()
    {
        handle(null);
    }
}
