package pl.rabbit.media.player.handlers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.util.Duration;
import pl.rabbit.media.player.controller.ProgressController;
import pl.rabbit.media.player.utils.TimeUtils;

public class SliderProgressListener implements ChangeListener<Duration>
{
    private final ProgressController progress;
    
    public SliderProgressListener(ProgressController progress)
    {
        this.progress = progress;
    }
    
    @Override
    public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue)
    {
        this.progress.getProgressSlider().setValue(newValue.toSeconds());
        this.progress.getTimeLabel().setText(TimeUtils.getTime(newValue));
    }
}
