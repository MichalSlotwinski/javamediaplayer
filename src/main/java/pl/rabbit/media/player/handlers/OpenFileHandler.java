package pl.rabbit.media.player.handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.view.ControlsView;
import pl.rabbit.media.player.view.MediaView;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

public class OpenFileHandler implements EventHandler<ActionEvent>
{
    private final MediaController controller;
    private final ControlsView controls;
    private final MediaView media;
    private final Stage primaryStage;
    private AtomicReference<MediaPlayer.Status> statusReference;
    
    
    public OpenFileHandler(MediaController controller, ControlsView controls, MediaView media, Stage primaryStage)
    {
        this.controller = controller;
        this.controls = controls;
        this.media = media;
        this.primaryStage = primaryStage;
        this.statusReference = null;
    }
    
    @Override
    public void handle(ActionEvent event)
    {
        this.statusReference = new AtomicReference<>();
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(this.primaryStage);
        if(file != null)
        {
            if(this.controller.player() != null)
                this.controller.player().stop();
            this.controller.open(file.toURI());
            this.media.setView(this.controller.view());
            this.media.setOnViewportWidthChangeAction(new ViewportWidthHandler(this.controller.view()));
            this.media.setOnViewportHeightChangeAction(new ViewportHeightHandler(this.controller.view()));
            this.controls.init();
            this.controller.player().setOnReady(
                    () ->
                    {
                        initPlay();
                        initProgress();
                        initVolume();
                    }
            );
        }
    }
    
    private void initPlay()
    {
        this.controls.getPlay().setOnPlayAction(new PlayHandler(this.controller, this.controls.getPlay(), this.media, this.controls.getProgress()));
        this.controls.getPlay().setOnStopAction(new StopHandler(this.controller, this.media, this.controls.getPlay(), this.controls.getProgress()));
        this.controller.player().setOnEndOfMedia(new StopHandler(this.controller, this.media, this.controls.getPlay(), this.controls.getProgress()));
    }
    
    private void initProgress()
    {
        this.controller.setOnProgressChangeListener(new SliderProgressListener(this.controls.getProgress()));
        this.controls.getProgress().setDuration(this.controller.media().getDuration());
        this.controls.getProgress().setOnClickedAction(new SliderClickedHandler(this.controller, this.controls.getProgress()));
        this.controls.getProgress().setOnPressedAction(new SliderPressedHandler(this.controller, this.statusReference));
        this.controls.getProgress().setOnReleasedAction(new SliderReleasedHandler(this.controller, this.statusReference));
        this.controls.getProgress().setOnDraggedAction(new SliderDraggedHandler(this.controller, this.controls.getProgress()));
    }
    
    private void initVolume()
    {
        this.controls.getVolume().setLimit(1);
        this.controls.getVolume().setOnMuteAction(new MuteHandler(this.controller, this.controls.getVolume()));
        this.controls.getVolume().getVolumeSlider().valueProperty().addListener(new VolumeChangeListener(this.controller, this.controls.getVolume()));
    }
}
