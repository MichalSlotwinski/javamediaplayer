package pl.rabbit.media.player.handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.MediaPlayer;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.controller.PlayController;
import pl.rabbit.media.player.controller.ProgressController;
import pl.rabbit.media.player.utils.TimeUtils;
import pl.rabbit.media.player.view.MediaView;

public class PlayHandler implements EventHandler<ActionEvent>
{
    private final MediaController media;
    private final PlayController play;
    private final MediaView view;
    private final ProgressController progress;
    
    public PlayHandler(MediaController media, PlayController play, MediaView view, ProgressController progress)
    {
        this.media = media;
        this.play = play;
        this.view = view;
        this.progress = progress;
    }
    
    @Override
    public void handle(ActionEvent event)
    {
        if(this.media.player().getStatus() == MediaPlayer.Status.PLAYING)
        {
            this.play.pause();
            this.media.player().pause();
        }
        else
        {
            this.play.play();
            this.view.rollbackView();
            this.media.player().play();
            this.progress.getTimeLabel().setText(TimeUtils.getTime(this.media.player().getCurrentTime()));
            this.progress.getDurationLabel().setText(TimeUtils.getTime(this.media.media().getDuration()));
        }
    }
}
