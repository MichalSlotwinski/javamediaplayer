package pl.rabbit.media.player.handlers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;

public class PaneWidthHandler implements ChangeListener<Number>
{
    private Pane pane;
    
    public PaneWidthHandler(Pane pane)
    {
        this.pane = pane;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue)
    {
        double value = newValue.doubleValue();
        this.pane.setMinWidth(0.0);
        this.pane.setPrefWidth(value);
        this.pane.setMaxWidth(value);
    }
}
