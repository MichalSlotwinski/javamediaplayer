package pl.rabbit.media.player.handlers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.controller.VolumeController;
import pl.rabbit.media.player.controller.enumarate.VolumeState;

public class VolumeChangeListener implements ChangeListener<Number>
{
    private final MediaController media;
    private final VolumeController volume;
    
    public VolumeChangeListener(MediaController media, VolumeController volume)
    {
        this.media = media;
        this.volume = volume;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
    {
        // set player volume
        this.media.player().setVolume(newValue.doubleValue());

        // set mute button look
        this.volume.getMuteButton().setText(
                VolumeState.stateOf(newValue.doubleValue(), this.volume.getLimit()).getText()
        );
        
        // set volume percentage text
        this.volume.getVolumeLabel().setText(
                VolumeController.VOLUME_PERCENTAGE_FORMAT.formatted((int)(newValue.doubleValue() * 100))
        );
    }
}
