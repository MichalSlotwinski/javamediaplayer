package pl.rabbit.media.player.handlers;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.MediaPlayer;
import pl.rabbit.media.player.controller.MediaController;

import java.util.concurrent.atomic.AtomicReference;

public class SliderReleasedHandler implements EventHandler<MouseEvent>
{
    private final MediaController media;
    private final AtomicReference<MediaPlayer.Status> reference;
    
    public SliderReleasedHandler(MediaController media, AtomicReference<MediaPlayer.Status> reference)
    {
        this.media = media;
        this.reference = reference;
    }
    
    @Override
    public void handle(MouseEvent event)
    {
        if(this.reference.get() == MediaPlayer.Status.PLAYING)
            this.media.player().play();
    }
}
