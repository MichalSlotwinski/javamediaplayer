package pl.rabbit.media.player.handlers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.media.MediaView;

public class ViewportWidthHandler implements ChangeListener<Number>
{
    private MediaView view;
    
    public ViewportWidthHandler(MediaView view)
    {
        this.view = view;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue)
    {
        this.view.setFitWidth(newValue.doubleValue());
    }
}
