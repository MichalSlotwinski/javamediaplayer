package pl.rabbit.media.player.handlers;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.controller.ProgressController;

public class SliderClickedHandler implements EventHandler<MouseEvent>
{
    private final MediaController media;
    private final ProgressController progress;
    
    public SliderClickedHandler(MediaController media, ProgressController progress)
    {
        this.media = media;
        this.progress = progress;
    }
    
    @Override
    public void handle(MouseEvent event)
    {
        this.media.player().seek(
                Duration.seconds(
                        this.progress.getProgressSlider().getValue()
                )
        );
    }
}
