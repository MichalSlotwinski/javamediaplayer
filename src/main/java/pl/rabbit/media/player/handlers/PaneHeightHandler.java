package pl.rabbit.media.player.handlers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;

public class PaneHeightHandler implements ChangeListener<Number>
{
    private Pane pane;
    
    public PaneHeightHandler(Pane pane)
    {
        this.pane = pane;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue)
    {
        double value = newValue.doubleValue();
        this.pane.setMinHeight(0.0);
        this.pane.setPrefHeight(value);
        this.pane.setMaxHeight(value);
    }
}
