package pl.rabbit.media.player.handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import pl.rabbit.media.player.controller.MediaController;
import pl.rabbit.media.player.controller.VolumeController;

public class MuteHandler implements EventHandler<ActionEvent>
{
    private final MediaController media;
    private final VolumeController volume;
    
    public MuteHandler(MediaController media, VolumeController volume)
    {
        this.media = media;
        this.volume = volume;
    }
    
    @Override
    public void handle(ActionEvent event)
    {
        if(this.media.player().isMute())
        {
            this.volume.setMute(false);
            this.media.player().setMute(false);
        }
        else
        {
            this.volume.setMute(true);
            this.media.player().setMute(true);
        }
    }
}
