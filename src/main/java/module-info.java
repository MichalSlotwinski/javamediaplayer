module pl.rabbit.media
{
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.media;
    exports pl.rabbit.media.player;
    exports pl.rabbit.media.player.handlers;
    exports pl.rabbit.media.player.view;
    exports pl.rabbit.media.player.controller;
    exports pl.rabbit.media.player.controller.enumarate;
}
