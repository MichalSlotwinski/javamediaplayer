# JavaMediaPlayer

Java based media player
### Build
```shell
mvn clean package
```
### Run
```shell
org.openjfx:javafx-maven-plugin:0.0.6:run
```
*Suggest to use maven archetype plugin to run project 

### References
* [OpenJFX docs](https://openjfx.io/openjfx-docs/#maven)

* [Maven archetype source](https://github.com/openjfx/javafx-maven-archetypes/tree/master/javafx-archetype-simple) (GitHub)

Maven archetype command
```shell
mvn archetype:generate \
        -DarchetypeGroupId=org.openjfx \
        -DarchetypeArtifactId=javafx-archetype-simple \
        -DarchetypeVersion=0.0.6 \
        -DgroupId=org.openjfx \
        -DartifactId=sample \
        -Dversion=0.1 \
        -Djavafx-version=17.0.0.1 \
        -Djavafx-maven-plugin-version=0.0.6
```